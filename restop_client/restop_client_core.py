__author__ = 'cocoon'
"""


        A local copy of  restop/client.py



"""
import json
import requests



#base_url= "http://localhost:5000"
#base_url= "http://localhost/restop/api/v1:5000"

headers= { 'Content-type': "application/json"}


class Oauth2(requests.auth.AuthBase):
    """
        add oauth2 authorization header

    """

    def __init__(self,acces_token=None):
        self.access_token= acces_token

    def __call__(self, r):
        # Implement my authentication
        if self.access_token:
            r.headers['Authorization']= "Bearer %s" % self.access_token
        return r


class RestopClient(object):
    """


    """
    default_token_query= dict( client_id= 'anonymus', client_secret= '', grant_type= 'client_credentials')


    def __init__(self,base_url, **parameters):
        """

        """
        self.base_url= base_url

        self.rs = requests.session()
        self.rs.headers.update({
            'Accept': 'application/json',
        })

        return

    def url_collection(self,collection):
        """

        :param collection:
        :return:
        """
        return "%s/%s" % (self.base_url,collection)

    def url_item(self,collection,item_id):
        """

        :param collection:
        :param item_id:
        :return:
        """
        return "%s/%s/%s" % (self.base_url,collection,str(item_id))

    def url_operation(self,collection,item_id,operation):
        """

        :param collection:
        :param item_id:
        :param operation:
        :return:
        """
        return "%s/%s/%s/%s" % (self.base_url,collection,str(item_id),operation)

    #
    #
    #
    # def _merge_headers(self,kwargs):
    #     _headers= headers.copy()
    #     try:
    #         h= kwargs.pop('headers')
    #         _headers.update(h)
    #     except KeyError:
    #         pass
    #     return _headers

    def get(self,url,data=None,headers= None , access_token=None,**kwargs):
        data= data or {}
        headers= headers or {}
        if data:
            data= json.dumps(data)
            headers.update( {'Content-type': "application/json"})
        if access_token:
            return self.rs.get(url,data=data,headers=headers,auth=Oauth2(access_token) , **kwargs)
        else:
            return self.rs.get(url,data=data,headers=headers,**kwargs)

    def post(self,url,data=None,headers= None , access_token=None, **kwargs):
        data= data or {}
        headers= headers or {}
        if data:
            data= json.dumps(data)
            headers.update( {'Content-type': "application/json"})
        if access_token:
            return self.rs.post(url,data=data,headers=headers,auth=Oauth2(access_token) , **kwargs)
        else:
            return self.rs.post(url,data=data,headers=headers,**kwargs)

    def put(self,url,data=None,headers= None ,access_token=None, **kwargs):
        data= data or {}
        headers= headers or {}
        if data:
            data= json.dumps(data)
            headers.update( {'Content-type': "application/json"})
        if access_token:
            return self.rs.put(url,data=data,headers=headers,auth=Oauth2(access_token) , **kwargs)
        else:
            return self.rs.put(url,data=data,headers=headers,**kwargs)

    def delete(self,url,data=None,headers= None, access_token=None, **kwargs):
        data= data or {}
        headers= headers or {}
        if data:
            data= json.dumps(data)
            headers.update( {'Content-type': "application/json"})
        if access_token:
            return self.rs.delete(url,data=data,headers=headers,auth=Oauth2(access_token) , **kwargs)
        else:
            return self.rs.delete(url,data=data,headers=headers,**kwargs)


    def download(self,url,headers= None , access_token=None,**kwargs):
        # read in stream mode
        headers= headers or {}
        headers.update( {'Accept-Ranges': "bytes"})
        if access_token:
            return self.rs.get(url,headers=headers,auth=Oauth2(access_token) ,stream=True, **kwargs)
        else:
            return self.rs.get(url,headers=headers,stream=True,**kwargs)

    #
    #
    #

    def item_new(self,collection,data,acces_token=None,**kwargs):
        """
        :param collection:
        :param data:
        :return:
        """
        r= self.post( self.url_collection(collection), data=data, access_token=None,**kwargs )
        return r


    def item_get(self,collection,item_id, access_token=None,**kwargs):
        """

        :param collection:
        :param data:
        :return:
        """
        r= self.get(self.url_item(collection,item_id), access_token=access_token,**kwargs)
        return r


    def item_put(self,collection,item_id,data, access_token=None,**kwargs):
        """
        :param collection:
        :param data:
        :return:
        """
        r= self.put( self.url_item(collection,item_id), data=data , access_token=access_token)
        return r


    def item_delete(self,collection,item_id, access_token=None,**kwargs):
        """

        :param collection:
        :param data:
        :return:
        """
        r= self.delete(self.url_item(collection,item_id), access_token=access_token,**kwargs)
        return r


    def operation_post(self,collection,item_id,operation, access_token=None,**kwargs):
        """

        :param collection:
        :param item_id:
        :param operation:
        :param kwargs:
        :return:
        """
        url= self.url_operation(collection,item_id,operation)
        return self.post(url,access_token=access_token, data=kwargs)


    def operation_get(self,collection,item_id,operation,access_token=None, **kwargs):
        """

            get an operation template

        :param collection:
        :param item_id:
        :param operation:
        :param kwargs:
        :return:
        """
        url= self.url_operation(collection,item_id,operation)
        return self.get(url,access_token=access_token,data=kwargs)


    def oauth2_get_token(self, client_id= None,client_secret=None,grant_type='client_credentials', **kwargs):
        """

        :return:
        """
        url= self.url_item('oauth2','token')

        data= self.default_token_query.copy()
        if client_id:
            data['client_id']= client_id
        if client_secret:
            data[client_secret]= client_secret


        data.update(kwargs)
        #data= json.dumps(data)
        r= self.post(url,data=data,headers=headers)
        r.encoding='utf-8'
        return r


    def oauth2_revoke(self,access_token, **kwargs):
        """

        :return:
        """
        url= self.url_item('oauth2','revoke')
        r= self.post(url,auth=Oauth2(access_token))
        r.encoding='utf-8'
        return r

    def upload_file(self,filepath,filename,access_token):
        """


        :param filepath:
        :param filename:
        :access_token:
        :return:
        """
        url= self.url_item('files',filename)

        files={'file': open(filepath,'rb')}

        #r= requests.post(url,auth= Oauth2(access_token ),files= files)
        r= self.post(url,auth= Oauth2(access_token ),files= files)
        r.encoding='utf-8'

        return r

    def download_file(self,filename,access_token):
        """


        :param filepath:
        :param category:
        :return:
        """
        url= self.url_item('files',filename)
        # send a Accept all request
        r= requests.get(url,auth= Oauth2(access_token ))
        r.encoding='utf-8'

        return r


class Oauth2ClientMixin(object):
    """

        REQUEST:
        =======

         POST /token HTTP/1.1
         Host: server.example.com
         Content-Type: application/json

         json_data:
         ---------
         grant_type=client_credentials
         client_id=johndoe
         clients_secret=A3ddj3w .....


        RESPONSE:
        ========


    """
    rs= None
    post=None
    url_item= None

    def oauth2_get_token(self,user='anonymus', **kwargs):
        """

        :return:
        """
        url= self.url_item('oauth2','token')
        data= { 'user': user}
        data.update(kwargs)
        data= json.dumps(data)
        r= self.post(url,data=data,headers=headers)
        r.encoding='utf-8'
        return r


    def oauth2_revoke(self,access_token, **kwargs):
        """

        :return:
        """
        url= self.url_item('oauth2','revoke')
        r= self.post(url,auth=Oauth2(access_token))
        r.encoding='utf-8'
        return r