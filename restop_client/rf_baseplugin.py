"""


    A base for robot-framework plugin library




"""

import inspect
from pprint import pprint
from http_proxy import SimpleProxy
from restop_api import RestopApi ,get_keywords
import yaml

import logging
log=logging.getLogger(__name__)


# default base url
base_url = "http://localhost:5000/restop/api/v1"

RESTOP_INI= "./restop_client.ini"


# from robot.libraries.BuiltIn import BuiltIn

#syp_ptf = dict(

#    # here comes the platform configuration
#)

# set to store all external keywords
#KEYWORDS = set()

class RobotFrameworkListener(object):
    """



    """
    ROBOT_LISTENER_API_VERSION = 2
    ROBOT_LIBRARY_SCOPE = 'Global'

    def __init__(self):
        """
            init pilot
        """
        self.ROBOT_LIBRARY_LISTENER = self

    def start_suite(self, name, attributes):
        #print("start suite with name=%s , attributes=%s" % (str(name),str(attributes)))
        pass

    def end_suite(self, name, attributes): pass

    def start_test(self, name, attributes): pass

    def end_test(self, name, attributes): pass

    def start_keyword(self, name, attributes): pass

    def end_keyword(self, name, attributes): pass

    def log_message(self, message): pass

    def message(self, message): pass

    def output_file(self, path): pass

    def log_file(self, path): pass

    def report_file(self, path): pass

    def debugfile(self, path): pass

    def close(self): pass

    # def _end_test(self, name, attrs):
    #         print 'Suite %s (%s) ending.' % (name, attrs['id'])
    #         self.test_status[name.lower()] = attrs["status"]


class Pilot(RobotFrameworkListener):
    """
        robot framework class library





    """
    #_keywords = KEYWORDS

    #_session_collection= 'phone_sessions'
    #_collection_operations= ['adb_devices', 'get_platform_configuration', 'ptf_get_user_data', 'get_sip_address']

    def __init__(self):
        """
            init pilot
        """
        super(Pilot, self).__init__()

        self._result = ''
        self._dry = False

        self._session_collection= None
        self._collection_operations=None
        self._keywords= set()

        # add own dynamic keywords
        for name, func in inspect.getmembers(Pilot, predicate=inspect.ismethod):
            if not name.startswith('_'):
                self._keywords.add(name)

        # try to load remote keywords
        self._api= RestopApi(RESTOP_INI)
        try:
            self._session_collection= self._api.session_collection
            self._collection_operations= self._api.collection_operations
            remote_keywords = self._api.operations
            for kw in remote_keywords:
                # update keywords with remote
                self._keywords.add(kw)
        except Exception, e:
            #print "%s in file restop_client.ini" % e.message
            pass



    # get_lib = BuiltIn().get_library_instance('Selenium2Library')


    def get_keyword_names(self):
        """

        :return:
        """
        return list(self._keywords)


    def set_pilot_dry_mode(self):
        """
            set pilot to dry mode ( no action will be taken towards devices)
        """
        print "pilot: warning: you enter in dry mode"
        self._dry = True

    def fetch_restop_interface(self, url):
        """
            fetch interface from remote restop server  GET /apidoc
            and save it to file restop_client.ini

        :param url:
        :return:
        """
        # request api doc and store it in restop_client.ini
        data= RestopApi.fetch(url,filepath=RESTOP_INI)
        if data:
            data= data['message']
        else:
            log.warning('cannot fetch restop interface')
        return data


    def setup_pilot(self, platform_name, platform_version, platform_url=None):
        """
            initialize pilot with platform configuration file
        """
        self.platform_name = platform_name
        self.platform_version = platform_version
        self.platform_url = platform_url or base_url

        print "pilot: setting up platform pilot for platform=%s , version=%s ,url=%s" % (
            platform_name, platform_version, platform_url)

        # try to find client configuration
        remote_keywords=None
        try:
            remote_keywords = get_keywords('restop_client.ini')
            for kw in remote_keywords:
                # update keywords with remote
                self._keywords.add(kw)

        except Exception ,e:
            print "%s in file restop_client.ini" %  e.message
            raise e
        #collection_operations = ['adb_devices', 'get_platform_configuration', 'ptf_get_user_data', 'get_sip_address']
        self.proxy = SimpleProxy(
            self.platform_url,
            collection= self._session_collection,   # 'phone_sessions',
            collection_operations=self._collection_operations
        )

        return

    def _dry_return(self, *args, **kwargs):
        """
            return with dry mode : always OK
        """
        print "pilot: warning you are in dry mode, no operation was transmitted."
        self._result = "OK"
        return self._result

    def open_Session(self, *users):
        """
            open a session : start session with a terminal per user specified

            Open Session Alice Bob

            -    start a session with userA=Alice , userB=Bob
        """
        #print "pilot: initialisation conf = %s " % str(self.conf)
        print "pilot: Open Session with users: %s" % str(users)

        if self._dry:
            return self._dry_return()

        result = self.proxy.open(users)
        # {u'agents_count': 2, u'agent_started': 2}

        message= self._handle_result(result)
        if not message == True:
            raise RuntimeError("can not start all agents")
        # pprint(result)
        # # check all agents are started
        # if result.has_key('code') and result['code'] >= 300:
        #     raise RuntimeError('cannot open session')
        # if not result['agent_started'] == result['agents_count']:
        #     raise RuntimeError("cant start all agents")


    def close_session(self, result=0, error=0):
        """
            close session , unregister all users and quit
        """
        print "pilot: Close Session"
        if self._dry:
            # self._session = None
            return self._dry_return()

        response_data = self.proxy.close()
        #response_data= response.json()
        message= self._handle_result(response_data)


    # primary interface
    def _execute(self, user, command, *args, **kwargs):
        """
           the transmission belt to proxy

        """
        if not self._dry:
            # return self._session.execute(user,command,*args,**kwargs)
            res = self.proxy._send_operation(command, user=user, **kwargs)
            if not isinstance(res,dict):
                # res is text
                res= dict(message='failed',result=500,logs=[res])
            return self._handle_result(res)

        else:
            return self._dry_return()

    def _get_url(self, user, command, *args, **kwargs):
        """


        """
        if not self._dry:
            # return self._session.execute(user,command,*args,**kwargs)
            res = self.proxy._get(command, user=user, **kwargs)
            return self._handle_result(res)

        else:
            return self._dry_return()


    def _handle_result(self, result, raise_on_error=True):
        """


        :param result: dict   { result: 200 , logs: [] , message: ""}
        :return:
        """
        status = result.get('result', 200)
        message = result.get('message', None)
        print '=== status:%s' % str(status)
        print '=== message: %s' % result.get('message', 'No message')
        print '=== logs:'
        pprint(result.get('logs', []))
        tb= result.get('tb',None)
        if tb:
            print("=== trace-back:")
            pprint(tb)
        # check status
        if raise_on_error:
            if isinstance(status, int):
                if status >= 400:
                    raise RuntimeError("code: %s , %s" % (str(status), str(message)))
        return message


    def apidoc(self, collection='-', item='-'):
        """

        :param collection:
        :return:
        """
        return self._get_url('-', 'apidoc', collection=collection, item=item)


    def upload_platform(self,filename='platform.yml'):
        """
            upload a platform.yml file to restop remote server

        :param filename:
        :return:
        """
        platform_file= file(filename,'rb')
        platform_data= yaml.load(platform_file)

        url=self.proxy.url_for(collection='platforms')
        r= self.proxy.client.post(url,data=platform_data)
        assert r.status_code == 200



    def get_platform_configuration(self):
        """
            Get platform configuration ( a platform.json)
        """
        result = self.proxy.get_platform_configuration()
        print "pilot: platform configuration is %s" % str(result)
        return result


    def dummy_operation(self):
        """
            A dummy operation:  do nothing
        """
        print "pilot: dummy operation"


    # mapping for the agent method ( click , wait_for_exists , press_home ... )
    def __getattr__(self, function_name):
        """
            wrapper to wild methods

        :param function_name:
        :return:
        """

        def wrapper(agent_id='-', **kwargs):
            """

            """
            return self._execute(agent_id, function_name, **kwargs)

        return wrapper




robot_plugin = Pilot

