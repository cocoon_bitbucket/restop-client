from distutils.core import setup

setup(
    name='restop-client',
    version='',
    packages=['restop_client'],
    url='',
    license='',
    author='cocoon',
    author_email='tordjman.laurent@gmail.com',
    description='a robotframework client to restop framework',
    scripts=['restop_client/batch.py']
)
