# -*- coding: utf-8 -*-

__author__ = 'cocoon'

import time

import os
from restop_client import Pilot



phonehub_url= "http://localhost:5000/restop/api/v1"

#phonehub_url= "http://192.168.99.1:5000/restop/api/v1"
#phonehub_url= "http://192.168.99.100:5000/restop/api/v1"
#phonehub_url= "http://10.179.1.246/restop/api/v1"

#phonehub_url= "http://192.168.99.100/restop/api/v1"


def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=phonehub_url)


    return data


def test_samples_basic():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session('sample_1','sample_2')


        try:
            res= p.raise_error('sample_1')
        except:
            pass

        try:
            res= p.application_error('sample_1')
        except:
            pass

        try:
            res = p.not_implemented('sample_1')
        except:
            pass

        res= p.dummy('sample_1')
        res= p.dummy('sample_2')

        # read alarm on samples
        time.sleep(2)

        res = p.read('sample_1')
        #assert res.startswith('Signal')
        res = p.read('sample_2')
        #assert res.startswith('Signal')


        res= p.ping('sample_1')
        time.sleep(2)

        res= p.read('sample_1')
        assert res=='pong\n'

    except RuntimeError,e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return



def test_upload_platform():
    """


    :return:
    """


    p = Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    r = p.upload_platform(filename='platform.yml')

    return



def basic():

    p =Pilot()

    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('Alice','Bob')


    try:

        res= p.call_user('Alice','Bob')

        res= p.answer_call_ok('Bob')

        time.sleep(2)

        res= p.hangup('Alice')

    except RuntimeError:
        print "interrupted"

    finally:
        r= p.close_session()


    r= p.close_session()

    return

def classic():
    """

    Call user   ${userA}   ${userB}   ${format}
    Wait incoming Call  ${userB}
    Answer call ringing     ${userB}
    Wait ringing    ${userA}
    Answer call ok     ${userB}

    Wait Call confirmed     ${userA}

    Hangup  ${userA}


    :return:
    """

    p =Pilot()

    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('Alice','Bob')

    try:
        res= p.call_user('Alice','Bob')
        res= p.wait_incoming_call('Bob')

        #res= p.answer_call_ringing('Bob')
        #res= p.watch_log('Alice')
        #res= p.watch_log('Bob')

        #res= p.wait_ringing('Alice')



        res= p.answer_call_ok('Bob')

        time.sleep(2)

        res= p.hangup('Alice')

    except RuntimeError:
        print "interrupted"

    finally:
        p.close_session()


def test_check_not_received():
    """

    Open session    ${userA}    ${userB}    ${userC}
    Call user   ${userA}   ${userB}     universal
    Wait incoming Call  ${userB}
    Answer call ringing     ${userB}
    #Wait ringing    ${userA}
    Answer call ok     ${userB}

    Wait Call confirmed     ${userA}

    check no incoming call received     ${userC}  ${refresh_count}

    Hangup  ${userA}



    :return:
    """

    p =Pilot()

    p.setup_pilot("demo","demo_qualif","dummy")

    #res= p.open_Session('Alice','Bob')
    res= p.open_Session('Alice','Bob','Charlie')
    time.sleep(3)
    try:

        res= p.call_user('Alice','Bob')
        res= p.wait_incoming_call('Bob')

        res= p.answer_call_ok('Bob')

        res= p.wait_call_confirmed('Alice')

        res= p.check_no_incoming_call_received('Charlie')

        time.sleep(2)

        res= p.hangup('Alice')

    except RuntimeError:
        print "interrupted"

    finally:
        p.close_session()


def test_simple_check():
    """

       ${caller_info}=  Ptf Get User Data  ${userA}
        Log Many  ${caller_info}
        check incoming call  ${userB}  display_name=${caller_info['display']}
        #check incoming call  ${userB}  display_name=${Alice_display}
        Answer call ok     ${userB}
        Wait Call confirmed     ${userA}


        hangup    ${userA}

    """

    p =Pilot()
    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('Alice','Bob')
    time.sleep(1)
    try:

        caller_info= p.ptf_get_user_data('Alice')

        res= p.call_user('Alice','Bob', format= 'ext')
        res= p.wait_incoming_call('Bob')

        try:
            res= p.check_incoming_call('Bob' , display_name='dummy')
        except Exception ,e:
            pass

        res= p.check_incoming_call('Bob' , display_name=caller_info['format_universal'])



        res= p.answer_call_ok('Bob')

        res= p.wait_call_confirmed('Alice')


        time.sleep(1)

        res= p.hangup('Alice')

    except RuntimeError:
        print "interrupted"

    finally:
        p.close_session()

def test_get_last_received_message():
    """

       ${caller_info}=  Ptf Get User Data  ${userA}
        Log Many  ${caller_info}
        check incoming call  ${userB}  display_name=${caller_info['display']}
        #check incoming call  ${userB}  display_name=${Alice_display}
        Answer call ok     ${userB}
        Wait Call confirmed     ${userA}


        hangup    ${userA}

    """

    p =Pilot()
    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('Alice','Bob')
    time.sleep(1)
    try:

        caller_info= p.ptf_get_user_data('Alice')

        res= p.call_user('Alice','Bob', format= 'ext')
        res= p.wait_incoming_call('Bob')
        res= p.answer_call_ok('Bob')

        sip_message= p.get_last_received_sip_request('Bob',sip_method='INVITE')



        res= p.wait_call_confirmed('Alice')


        time.sleep(1)

        res= p.hangup('Alice')

    except RuntimeError:
        print "interrupted"

    finally:
        p.close_session()



def test_real_android_phones():
    """


    :return:
    """

    Mobby_number=   '+33784109762'
    Marylin_number= '+33784109764'


    p =Pilot()
    p.setup_pilot("demo","demo_qualif","dummy")



    res= p.open_Session('Mobby','Marylin')
    time.sleep(1)
    try:

        #res= p.ptf_get_user_data('Mobby')

        #res= p.wait_incoming_call('Marylin')
        #res= p.call_number('Mobby',Marylin_number)

        res= p.call_user('Mobby','Marylin')
        #time.sleep(10)

        res= p.wait_incoming_call('Marylin')
        res= p.answer_call_ok('Marylin')

        res= p.wait_call_confirmed('Marylin')
        res= p.wait_call_confirmed('Mobby')

        time.sleep(5)

        res= p.hangup('Mobby')

        res= p.wait_call_disconnected('Marylin')
        res= p.wait_call_disconnected('Mobby')


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()


def test_real_all_phones():
    """


    :return:
    """

    p =Pilot()
    p.setup_pilot("demo","demo_qualif","dummy")



    res= p.open_Session( 'Alice','Bob','Mobby','Marylin')
    time.sleep(3)
    try:


        # mobile one call mobile two
        res= p.call_user('Mobby','Marylin')

        # softphone one call softphone two
        res= p.call_user('Alice','Bob')


        # softphone two wait for incoming call and answer
        res= p.wait_incoming_call('Bob')
        res= p.answer_call_ok('Bob')


        # mobile one wait for incoming call and answer
        res= p.wait_incoming_call('Marylin')
        res= p.answer_call_ok('Marylin')


        # checks all calls confirmed
        res= p.wait_call_confirmed('Alice')
        #res= p.wait_call_confirmed('Bob')
        res= p.wait_call_confirmed('Marylin')
        res= p.wait_call_confirmed('Mobby')

        # in communication for delay
        time.sleep(5)

        # hangup mobile one
        res= p.hangup('Mobby')
        # hangup softphone one
        res= p.hangup('Alice')


        # check all calls disconnected
        #res= p.wait_call_disconnected('Alice')
        res= p.wait_call_disconnected('Bob')
        res= p.wait_call_disconnected('Marylin')
        res= p.wait_call_disconnected('Mobby')


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()




def macro_Activate_redirection_to_user(pilot,userA,userB,redirect_kind='CFA',call_format='universal'):
    """

    Macro Activate Redirection To User  ${userA}  ${userB}  ${redirect_kind}  ${call_format}

        activate redirection to user   ${userA}  ${userB}  ${redirect_kind}  ${call_format}
        wait call confirmed  ${userA}
        Wait Hangup  ${userA}

    """
    res= pilot.activate_redirection_to_user(userA=userA,userB=userB,redirect_kind=redirect_kind,call_format=call_format)
    res= pilot.wait_call_confirmed(userA)
    res= pilot.wait_hangup(userA)
    return

def macro_cancel_redirection(pilot,user,fac_kind='CFA'):
    """

    Macro Cancel Redirection
        [arguments]     ${user}    ${FAC_kind}
        [Documentation]     cancel a redirection  (kind can be CFA,CFB,CFNA,CFNR)
        Cancel Redirection   ${user}    ${FAC_kind}
        wait call confirmed  ${user}
        Wait Hangup  ${user}

    """
    res= pilot.cancel_redirection(user=user,redirect_kind=fac_kind)
    res= pilot.wait_call_confirmed(user)
    res= pilot.wait_hangup(user)
    return




def test_redirection():
    """

    redirect call always
    [Tags]  redirect always
    [template]  Unit A Call B redirected to C
    Aline  Bruce  Bruce_InterSite



    Macro Activate Redirection To User
        [Documentation]     redirect A to B with a kind of redirection and call format
        [arguments]     ${userA}  ${userB}  ${redirect_kind}=CFA  ${call_format}=universal

        activate redirection to user   ${userA}  ${userB}  ${redirect_kind}  ${call_format}
        wait call confirmed  ${userA}
        Wait Hangup  ${userA}

    Macro Cancel Redirection
        [arguments]     ${user}    ${FAC_kind}
        [Documentation]     cancel a redirection  (kind can be CFA,CFB,CFNA,CFNR)
        Cancel Redirection   ${user}    ${FAC_kind}
        wait call confirmed  ${user}
        Wait Hangup  ${user}


    Unit A Call B redirected to C
        [arguments]     ${userA}    ${userB}    ${userC}  ${redirect_kind}=CFA  ${call_format}=universal
        [Documentation]     $userA call $userB with a specific $format

        Open session     ${userA}    ${userB}   ${userC}

        # activate redirection for B to C
        Macro Activate Redirection To User  ${userB}  ${userC}  ${redirect_kind}  ${call_format}

        # A call B , C answers
        Call user   ${userA}   ${userB}   ${call_format}
        Wait incoming Call  ${userC}
        Answer call ok     ${userC}
        Wait Call confirmed     ${userA}
        watch log  ${userA}  1
        Hangup  ${userA}

        # cancel redirection for B
        Macro Cancel Redirection  ${userB}    ${redirect_kind}



    :return:
    """
    p =Pilot()
    p.setup_pilot("demo","demo_qualif","dummy")



    res= p.open_Session( 'Aline',  'Bruce',  'Bruce_InterSite')
    try:


        # activate redirection B to C
        macro_Activate_redirection_to_user(p,'Bruce','Bruce_InterSite')

        time.sleep(3)

        # a call B redirected to C
        res= p.call_user('Aline',  'Bruce')

        #
        res= p.wait_incoming_call('Bruce_InterSite')
        res= p.answer_call_ok('Bruce_InterSite')

        time.sleep(2)

        res= p.hangup('Aline')

        time.sleep(3)


        macro_cancel_redirection(p,'Bruce','CFA')

        # res= p.call_user('Aline',  'Bruce')
        # res= p.wait_incoming_call('Bruce')
        #
        # res= p.answer_call_ok('Bruce')
        #
        # time.sleep(2)
        #
        # res= p.hangup('Aline')



    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()


def test_redirection_2():


    p =Pilot()
    p.setup_pilot("demo","demo_qualif","dummy")


    try:

        #
        #   set redirection  B to C
        #

        res= p.open_Session( 'Bruce','Bruce_InterSite')
        #time.sleep(5)
        # activate redirection B to C
        macro_Activate_redirection_to_user(p,'Bruce','Bruce_InterSite')
        p.close_session()



        #
        #   A call B , redirected to C
        #

        res= p.open_Session( 'Aline',  'Bruce',  'Bruce_InterSite')
        #time.sleep(5)

       # a call B redirected to C
        res= p.call_user('Aline',  'Bruce')

        res= p.wait_incoming_call('Bruce_InterSite')
        res= p.answer_call_ok('Bruce_InterSite')

        time.sleep(2)

        res= p.hangup('Aline')

        p.close_session()

        #
        #  cancel redirection
        #

        res= p.open_Session( 'Bruce')
        #time.sleep(5)
        # activate redirection B to C
        macro_cancel_redirection(p,'Bruce')
        p.close_session()


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()



def test_destination():

    p =Pilot()

    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('Alice','Bob')


    try:

        #res= p.call_user('Alice','Bob')
        res= p.call_destination('Alice','oms_service')
        #res= p.watch_log('Alice',5)

        res= p.wait_incoming_call('Bob')
        res= p.answer_call_ok('Bob')

        time.sleep(2)

        res= p.hangup('Alice')

    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        r= p.close_session()


    r= p.close_session()

    return


def test_call_with_media():
    """
Unit Call With Media
    [arguments]     ${userA}    ${userB}    ${format}
    [Documentation]     $userA call $userB with a specific $format

    Open session    ${userA}    ${userB}
    Call user   ${userA}   ${userB}   ${format}
    Wait incoming Call  ${userB}
    Answer call ok     ${userB}
    Wait Call confirmed     ${userA}



    Start Player    ${userA}
    Watch Log       ${userA}  1

    Start Recorder  ${userB}
    Watch Log       ${userB}  5

    dump call_quality_status  ${userA}
    check_call_quality  ${userB}  min_packets=100  max_loss_rate=0.01  channel=RX

    Stop Recorder  ${userB}
    Watch Log      ${userB}  1

    Stop Player    ${userA}
    Watch Log      ${userA}  1


    Hangup  ${userA}
    #Close Session


    :return:
    """

    p =Pilot()

    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('Alice','Bob')


    try:

        res= p.call_user('Alice','Bob')

        res= p.answer_call_ok('Bob')

        p.start_player('Alice')   # Start Player    ${userA}
        p.watch_log('Alice')      # Log       ${userA}  1

        p.start_recorder('Bob')     # Recorder  ${userB}
        p.watch_log('Bob')          # Log       ${userB}  5

        p.dump_call_quality_status('Alice')     # call_quality_status  ${userA}
        p.check_call_quality('Bob')             #check_call_quality  ${userB}  min_packets=100  max_loss_rate=0.01  channel=RX

        p.stop_recorder('Bob')      # Stop Recorder  ${userB}
        p.watch_log('Bob')          # Watch Log      ${userB}  1

        p.stop_player('Alice')      #Stop Player    ${userA}
        p. watch_log('Alice')       #Watch Log      ${userA}  1


        res= p.hangup('Alice')

    except RuntimeError:
        print "interrupted"

    finally:
        r= p.close_session()


    r= p.close_session()

    return


def test_unit_simple_call():
    """
Unit Simple Call
	[arguments] 	${userA}	${userB} 	${format}
	[Documentation] 	$userA call $userB with a specific $format

	Open session	${userA}	${userB}
    Call user   ${userA}   ${userB}   ${format}
    Wait incoming Call  ${userB}
    Answer call ringing     ${userB}
    Wait ringing    ${userA}
    Answer call ok     ${userB}

    Wait Call confirmed     ${userA}

    Hangup  ${userA}



    :return:
    """

    p =Pilot()

    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('Alice','Bob')

    try:
        p.call_user('Alice','Bob')
        p.wait_incoming_call('Bob')

        #time.sleep(0.5)
        #p.answer_call_ringing('Bob')
        #
        # res= p.watch_log('Bob')
        # res= p.watch_log('Alice')
        # #res= p.wait_ringing('Alice')

        p.answer_call_ok('Bob')

        p.wait_call_confirmed('Alice')

        time.sleep(2)

        p.hangup('Alice')
        p.watch_log('Bob',2)

    except RuntimeError:
        print "interrupted"

    finally:
        p.close_session()



def test_peer_to_peer():
    """
U
    :return:
    """

    p =Pilot()

    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('Fred','Fanny')

    try:
        #p.call_user('Fred','Alice')
        p.call_number('Fred',"sip:192.168.1.50:5062")
        p.watch_log('Fred',2)

        p.wait_incoming_call('Fanny')
        p.answer_call_ok('Fanny')

        p.wait_call_confirmed('Fred')

        time.sleep(2)

        p.hangup('Fred')
        p.watch_log('Fred',2)

    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()


def test_android_p2p():
    """



    :return:
    """

    p =Pilot()
    p.setup_pilot("demo","demo_qualif","dummy")



    res= p.open_Session( 'Fred','Fanny','Mobby','Marylin')
    time.sleep(3)
    try:


        # mobile one call mobile two
        res= p.call_user('Mobby','Marylin')

        # softphone one call softphone two
        res= p.call_user('Alice','Bob')


        # softphone two wait for incoming call and answer
        res= p.wait_incoming_call('Bob')
        res= p.answer_call_ok('Bob')


        # mobile one wait for incoming call and answer
        res= p.wait_incoming_call('Marylin')
        res= p.answer_call_ok('Marylin')


        # checks all calls confirmed
        res= p.wait_call_confirmed('Alice')
        #res= p.wait_call_confirmed('Bob')
        res= p.wait_call_confirmed('Marylin')
        res= p.wait_call_confirmed('Mobby')

        # in communication for delay
        time.sleep(5)

        # hangup mobile one
        res= p.hangup('Mobby')
        # hangup softphone one
        res= p.hangup('Alice')


        # check all calls disconnected
        #res= p.wait_call_disconnected('Alice')
        res= p.wait_call_disconnected('Bob')
        res= p.wait_call_disconnected('Marylin')
        res= p.wait_call_disconnected('Mobby')


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()



def test_introspection():
    """
U
    :return:
    """

    p =Pilot()

    p.setup_pilot("demo","demo_qualif","dummy")


    try:

        res= p.open_Session()
        time.sleep(1)

        res= p.get_platform_configuration()



        try:
            res= p.raw_operation()
        except AttributeError:
            pass

        res= p.apidoc(collection='syprunner_agents',item='-')
        res= p.apidoc(collection='droydrunner_agents',item='-')


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()
        pass


def test_sma():
    """


    :return:
    """
    p =Pilot()
    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('Mobby','Marylin','LB1')
    time.sleep(1)
    try:

        res= p.livebox_echo("LB1",msg="my message")

        try:
            res= p.livebox_no_methods("LB1")
        except Exception,e:
            print e.message
            pass

        try:
            res= p.livebox_raise_exception("LB1")
        except Exception,e:
            print e.message
            pass


        res= p.call_user('Mobby','Marylin')
        #time.sleep(10)

        res= p.wait_incoming_call('Marylin')
        res= p.answer_call_ok('Marylin')

        res= p.wait_call_confirmed('Marylin')
        res= p.wait_call_confirmed('Mobby')

        time.sleep(5)

        res= p.hangup('Mobby')

        res= p.wait_call_disconnected('Marylin')
        res= p.wait_call_disconnected('Mobby')


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()


def test_sma_new():
    """


    :return:
    """
    p =Pilot()
    p.setup_pilot("demo","demo_qualif","dummy")

    res= p.open_Session('LB1','tv')
    time.sleep(1)
    try:

        res= p.livebox_echo("LB1",msg="my message")

        try:
            res= p.livebox_no_methods("LB1")
        except Exception,e:
            print e.message
            pass

        try:
            res= p.livebox_raise_exception("LB1")
        except Exception,e:
            print e.message
            pass

        #res= p.send_key("tv",key="VOD")


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()


def test_tvbox():

    p =Pilot()
    p.setup_pilot("demo","demo_qualif",phonehub_url)

    res= p.open_Session('tv')
    time.sleep(1)
    try:

        # sync/clear serial log
        r= p.serial_synch('tv')

        #select VOD
        r= p.send_key('tv',key='VOD')

        # down to 'personal suggestions'
        r= p.send_key('tv',key='Down',timeout=2)

        # select 2nd choice on horizontal list
        r= p.send_key('tv',key='Right',timeout=2)
        r= p.send_key('tv',key='Right',timeout=2)


        # select the film
        r= p.send_key('tv',key='OK')

        # select bande-annonce option
        r= p.send_key('tv',key='Down',timeout=2)
        r= p.send_key('tv',key='Down',timeout=2)

        # launch film
        r= p.send_key('tv',key='OK')

        # watch serial log for 15s
        #r= p.watch_log('tv',duration=15)


        # try:
        #     res= p.livebox_no_methods("tv")
        # except Exception,e:
        #     print e.message
        #     pass


        #res= p.send_key("tv",key="VOD")


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)


    def capture_log1():
        """



        :return:
        """
def capture_tvbox():

    p =Pilot()
    p.setup_pilot("demo","demo_qualif",phonehub_url)

    res= p.open_Session('tv')
    time.sleep(1)

    logs=[]

    try:

        # start tvbox with physical power button ( wait for completion)
        r= p.serial_synch('tv')
        logs.append(r)

        # send power off button
        r= p.send_key('tv','POWER')
        r= p.serial_synch("tv")
        logs.append(r)

        # send power ON button
        r= p.send_key('tv','POWER')
        r= p.serial_synch("tv")
        logs.append(r)


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)


    print logs
    with open("log_cold_start.tx","w") as fh:
        fh.write(logs[0])

    with open("log_soft_stop.tx","w") as fh:
        fh.write(logs[1])

    with open("log_soft_start.tx","w") as fh:
        fh.write(logs[2])


    return


def test_dump():
    """

    :return:
    """

    p =Pilot()
    p.setup_pilot("demo","demo_qualif",phonehub_url)

    res= p.open_Session('tv')
    time.sleep(1)

    logs=[]

    try:

        r= p.serial_synch('tv')

        r=p.stb_send_dump('tv')

        #r= p.send_cmd('tv',cmd='ls /tmp\n')

        r= p.serial_synch('tv')



    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)

def test_droyd():
    """



    :return:
    """
    p =Pilot()
    p.setup_pilot("demo","demo_qualif",phonehub_url)



    res= p.open_Session( 'Mobby','Marylin')
    time.sleep(3)
    try:


        # press home on Mobby
        r= p.press_home('Mobby')

        # mobile one call mobile two
        res= p.call_user('Mobby',userX='Marylin')

        # mobile one wait for incoming call and answer
        res= p.wait_incoming_call('Marylin')
        res= p.answer_call_ok('Marylin')

        # checks all calls confirmed
        res= p.wait_call_confirmed('Marylin')
        res= p.wait_call_confirmed('Mobby')

        # in communication for delay
        time.sleep(5)

        # hangup mobile one
        res= p.hangup('Mobby')


        # check all calls disconnected
        res= p.wait_call_disconnected('Marylin')
        res= p.wait_call_disconnected('Mobby')


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()

def test_scenario():
    """


    :return:
    """


    p = Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    res = p.open_Session('tv')
    time.sleep(1)

    logs = []

    try:


        #r = p.stb_send_dump('tv')

        r= p.send_key('tv',key="KEY_CHANNELUP")
        # time.sleep(1)
        r=p.send_key('tv',"KEY_INFO")
        # time.sleep(1)
        r = p.stb_send_dump('tv')
        time.sleep(1)
        r = p.page_get_info_from_live_banner('tv')


    except RuntimeError, e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)



def test_first():
    """

    set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
    :return:
    """


    p = Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    res = p.open_Session('tv')
    time.sleep(1)

    logs = []

    try:

        # dump stb serial
        #r = p.serial_synch('tv',timeout=1000)
        #r = p.serial_watch('tv',timeout= 5)

        # start stb scheduler
        #r= p.scheduler_start('tv')


        #
        # select channel 1
        #
        r= p.send_key('tv',key="1")
        r= p.serial_watch('tv',timeout=1)
        #time.sleep(2)

        r= p.stb_send_dump('tv')
        r=p.serial_watch('tv',timeout=1)
        #time.sleep(2)

        #r= p.page_get_info_from_live_banner('tv')
        # 'TF1'
        #print r['channelName']
        #print r['program']

        r= p.serial_synch('tv')

        # r=p.send_key('tv', key='PGM_P')
        # time.sleep(1)

        r = p.send_key('tv', key='2')
        r=p.serial_watch('tv',timeout=1)
        #time.sleep(5)

        r= p.stb_send_dump('tv')
        #time.sleep(2)
        r=p.serial_watch('tv',timeout=2)

        r= p.page_get_info_from_live_banner('tv')
        # 'FRANCE 2'
        print r['channelName']
        print r['program']



        # r = p.send_key('tv', key='PLAY_PAUSE')
        # # time.sleep(300)
        # r = p.send_key('tv', key='PLAY_PAUSE')
        # # time.sleep(600)
        # r = p.send_key('tv', key='MENU')
        # # time.sleep(10)


        # dump stb serial
        r = p.serial_synch('tv')

        # start stb scheduler
        #stats = p.scheduler_read('tv')

        # dump stb serial
        #r = p.serial_synch('tv')



    except RuntimeError, e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print  "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)


def test_second():
    """

    set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
    :return:
    """


    p = Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    res = p.open_Session('tv')
    time.sleep(1)

    logs = []

    try:

        r = p.serial_watch('tv',timeout= 2)


        # menu select
        r= p.send_key('tv' , key="MENU")
        r= p.serial_watch('tv',timeout=2)

        # select musique/videos
        r= p.menu_select('tv',path='4/1/1')
        r= p.serial_watch('tv',timeout=2)

        r= p.send_key('tv',key="OK")
        r = p.serial_watch('tv', timeout=5)
        r = p.send_key('tv', key="MENU")
        r = p.send_key('tv', key="MENU")


        #
        # select channel 1
        #
        r= p.send_key('tv',key="1")
        r= p.serial_watch('tv',timeout=2)


        r= p.stb_send_dump('tv')
        r=p.serial_watch('tv',timeout=2)


        r= p.page_get_info_from_live_banner('tv')
        # 'TF1'
        print r['channelName']
        print r['program']

        r= p.serial_synch('tv')



        # r = p.send_key('tv', key='PLAY_PAUSE')
        # # time.sleep(300)
        # r = p.send_key('tv', key='PLAY_PAUSE')
        # # time.sleep(600)
        # r = p.send_key('tv', key='MENU')
        # # time.sleep(10)


        # dump stb serial
        r = p.serial_synch('tv')

        # start stb scheduler
        stats = p.scheduler_read('tv')

        # dump stb serial
        r = p.serial_synch('tv')



    except RuntimeError, e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print  "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)


def test_serial_expect():
    """

    set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
    :return:
    """


    p = Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    res = p.open_Session('tv')
    time.sleep(1)

    logs = []

    try:

        r = p.serial_watch('tv',timeout= 2)

        r= p.send_key('tv',key='1')
        time.sleep(2)
        r = p.send_key('tv', key='3')
        time.sleep(2)

        r = p.send_key('tv', key='1')
        time.sleep(2)
        r = p.send_key('tv', key='3')
        time.sleep(2)

        r= p.send_key('tv',key='1')
        time.sleep(2)
        r = p.send_key('tv', key='3')
        time.sleep(2)


        r= p.send_key('tv',key='1')
        time.sleep(2)

        # u'root: [1;37m[DEBUG] app.controller> stb event [1] (count: 1)...[0m'
        r= p.serial_expect('tv',pattern='app.controller> stb event [1]',timeout = 10)


    except RuntimeError, e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print  "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)

    return



def test_stb_toolbox():
    """

    set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
    :return:
    """


    p = Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    res = p.open_Session('tv')
    time.sleep(1)

    logs = []

    try:

        r = p.serial_watch('tv',timeout= 2)

        r= p.send_key('tv',key='1')
        time.sleep(2)

        r= p.send_key('tv',key='2')
        time.sleep(2)


        r = p.send_key('tv', key='OK')
        time.sleep(2)


        r = p.stb_send_dump('tv')
        #r= p.page_get_info_from_live_banner('tv')

        r= p.stb_toolbox_list('tv')
        r= p.stb_toolbox_select('tv',text='langue',confirm=True)

        time.sleep(3)
        r=p.send_key('tv',key='KEY_BACK')

        #r= p.page_get_list('tv')
        #current= p.page_get_label_from_list_focus('tv')
        #resume = p.page_find_in_list('tv',t=u'résumé')


    except RuntimeError, e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print  "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)

    return


def test_scheduler():
    """

    set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
    :return:
    """


    p = Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    res = p.open_Session('tv')
    time.sleep(1)

    logs = []

    try:

        #r= p.stb_get_lineup('tv')


        # start stb scheduler
        r= p.scheduler_init('tv')


        #r= p.stb_status('tv')

        #
        # select channel 1
        #
        #r= p.send_key('tv',key="2")
        #r= p.serial_watch('tv',timeout=2)
        #time.sleep(2)

        #r= p.stb_send_dump('tv')
        #r=p.serial_watch('tv',timeout=2)
        #time.sleep(2)

        #r= p.page_get_info_from_live_banner('tv')
        # 'TF1'
        #print r['channelName']
        #print r['program']

        #r= p.serial_synch('tv')
        time.sleep(20)

        # dump stb serial
        r = p.serial_synch('tv')

        # start stb scheduler
        #stats = p.scheduler_read('tv')
        r= p.scheduler_sync('tv')

        # dump stb serial
        r = p.serial_synch('tv')
        time.sleep(15)


        r= p.scheduler_close('tv')
        time.sleep(15)

        r = p.serial_synch('tv')

    except RuntimeError, e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print  "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)


def test_get_lineup():
    """

    set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
    :return:
    """


    p = Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    res = p.open_Session('tv')
    time.sleep(1)

    logs = []

    try:

        # dump stb serial
        r = p.serial_synch('tv')

        r= p.stb_get_lineup('tv')

        # dump stb serial
        r = p.serial_synch('tv')


        time.sleep(15)

    except RuntimeError, e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print  "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)


def test_stb_info():
    """

    set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
    :return:
    """


    p = Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res = p.open_Session('tv')
        time.sleep(1)

        logs = []

        # dump stb serial
        r = p.serial_synch('tv')

        r= p.stb_info('tv')

        # dump stb serial
        r = p.serial_synch('tv')


        time.sleep(15)

    except RuntimeError, e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print  "interrupted: %s" % e.message

    finally:
        p.close_session()
        time.sleep(3)


#
#
#

def test_dummy_tvbox():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= 'tv'

    try:

        res= p.open_Session(user1)


        res= p.dummy(user1)

        # read alarm on samples
        time.sleep(2)

        res= p.serial_expect(user1,pattern= 'feedback/ardom')

        res = p.serial_synch(user1)
        #assert res.startswith('Signal')

        time.sleep(2)

        #res= p.ping(user1)
        #time.sleep(2)

        #res= p.read(user1)
        #assert res=='pong\n'

    except RuntimeError,e:
        print "interrupted"

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return






if __name__=="__main__":


    test_fetch_restop_interface()
    #test_upload_platform()


    #basic()
    #classic()

    #test_check_not_received()

    #test_simple_check()

    #test_get_last_received_message()

    #test_real_android_phones()

    #test_real_all_phones()

    #test_redirection()

    #test_redirection_2()

    #test_destination()



    #test_peer_to_peer()
    #test_call_with_media()

    #test_unit_simple_call()

    #test_introspection()


    #test_sma()

    #test_sma_new()

    #test_tvbox()
    #capture_tvbox()

    #test_dump()

    #test_scenario()



    #test_first()
    #test_second()

    #test_serial_expect()

    #test_stb_toolbox()
    #test_scheduler()

    #test_get_lineup()

    #test_stb_info()




    test_samples_basic()
    #test_dummy_tvbox()
    #test_droyd()



    print "Done."
