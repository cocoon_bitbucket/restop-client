"""

    unit tests for restop_client.rf_baseplugin.Pilot


    get_keyword_names
    fetch_restop_interface
    open_Session
    close_session
    _execute
    _get_url
    _handle_result
    apidoc
    upload_platform
    get_platform_configuration

    *wild*





"""
import requests
from restop_client.http_proxy import ProxySession


platform_name= "demo"
platform_version= "demo"
platform_url= 'http://localhost:5000/restop/api/v1'



result_ok = dict( result=200,message='OK',logs=[])
result_ko = dict( result=500 , message='KO', logs=['problem',])
result_bad = None




def test_basic_interface(local_pilot):

    assert local_pilot._api is not None
    assert local_pilot._dry == False

    for name in ['get_keyword_names', 'set_pilot_dry_mode',
                 'open_Session','close_session',
                 '_execute','_handle_result','_get_url'
                 ]:
        # get attribute and check it is not a wrapper
        a = getattr(local_pilot,name)
        assert a.__name__== name


    assert local_pilot.platform_name == platform_name
    assert local_pilot.platform_version == platform_version
    assert local_pilot.platform_url == platform_url

    assert local_pilot.proxy
    assert isinstance(local_pilot.proxy,ProxySession)

def test_wild_attribute(local_pilot):

    r= local_pilot.unknown_attribute
    # check unknown_attribute is a wrapper
    assert type(r).__name__ == 'function'



def test_get_keywords(local_pilot):

    keywords=local_pilot.get_keyword_names()
    assert len(keywords) > 0

def test_get_url_dry_mode_on(local_pilot):

    local_pilot.set_pilot_dry_mode()
    assert local_pilot._dry == True

    assert local_pilot._get_url('Alice','do_nothing') == 'OK'

def test_get_url_outside_a_session(local_pilot):

    assert local_pilot._dry == False

    try:
        r= local_pilot._get_url('Alice','do_nothing')

    except RuntimeError ,e:

        assert e.message == 'no current session'
    else:
        assert False, "should have raise a RuntimeError no current session"


def test_handle_result_ok(local_pilot):

    r= local_pilot._handle_result(result_ok,raise_on_error=True)
    assert r == result_ok['message']


def test_handle_result_ko_with_raise(local_pilot):

    try:
        r= local_pilot._handle_result(result_ko,raise_on_error=True)
    except RuntimeError,e:
        # RuntimeError: code: 500 , KO
        assert True
    else:
        assert False, "should have raise a RuntimeError"

def test_handle_result_ko_without_raise(local_pilot):

    r= local_pilot._handle_result(result_ko,raise_on_error=False)
    assert r == result_ko['message']



def test_execute_dry_mode(local_pilot):

    local_pilot.set_pilot_dry_mode()
    r= local_pilot._execute('Alice','dummy')
    assert r =='OK'
    # def _execute(self, user, command, *args, **kwargs):

def test_execute_outside_a_session(local_pilot):

    assert local_pilot._dry == False

    try:
        r= local_pilot._execute('Alice','do_nothing')

    except RuntimeError ,e:

        assert e.message == 'no current session'
    else:
        assert False, "should have raise a RuntimeError no current session"


def test_open_session(local_pilot):

    assert local_pilot._dry == False

    try:
        session= local_pilot.open_Session('Alice','Bob')
    except requests.ConnectionError:
        pass
    else:
        assert False, "should have raise a Http ConnectionError"

def test_open_session_dry_mode(local_pilot):

    local_pilot.set_pilot_dry_mode()
    session= local_pilot.open_Session('Alice','Bob')
    assert session == 'OK'

def test_close_session(local_pilot):

    assert local_pilot._dry == False

    r= local_pilot.close_session()
    assert r is None
    # try:
    #     session= local_pilot.close_session()
    # except RuntimeError, e:
    #     assert e.message == 'no current session'
    # else:
    #     assert False, "should have raise a RuntimeError no current session"

def test_close_session_dry_mode(local_pilot):

    local_pilot.set_pilot_dry_mode()
    session= local_pilot.close_session()
    assert session == 'OK'